import logging
import sys
import argparse
import glob, os
import time
from os.path import abspath, join, dirname, pardir
sys.path.append(join(abspath(dirname(__file__)), pardir))
import torch
from Common.utils.bee_utils.testfuncs import print_args, get_device, get_model_list
from Common.model_engine import ModelEngine
torch.backends.cudnn.deterministic = True


def parse_args(argv):
    parser = argparse.ArgumentParser(description="Decoder arguments parser.")
    parser.add_argument('-i', '--input', type=str, help='Input image file path.', default=None)
    parser.add_argument('--inputPath', type=str, help='Input image file path.', default=None)
    parser.add_argument('-o', '--output', type=str, default=None, help='Output bin file name')
    parser.add_argument('--outputPath', type=str, default='bitstreams', help='Output bin file path')
    parser.add_argument('--device', type=str, choices=['cpu', 'cuda'], default='cuda', help='CPU or GPU device.')
    parser.add_argument("--ckptdir", type=str, help='Checkpoint folder containing multiple pretrained models.')
    parser.add_argument("--cfg", type=str, help='Path to the CfG file', default = "Encoder/AllRecipes.json")
    parser.add_argument("--target_rate", type=float, nargs='+', default=[0.75,0.50,0.25,0.12,0.06], help="Target bpp (default: %(default)s)")

    # NIC Arguments
    parser.add_argument("-m", "--model", type=int, default=None, help="Model Index")
    parser.add_argument("--lambda_rd", type=float, default=1, help="Input lambda for variable-rate models")

    args = parser.parse_args(argv)
    return args


def encode(args):
    device = get_device(args.device)
    if args.input == None and args.inputPath == None:
        print("either the input image name or the folder path needs to be provided.")
        return []
    if args.ckptdir == None:
        print("either the ckptdir needs to be provided.")
        return []
    if args.inputPath:
        images = glob.glob(os.path.join(args.inputPath, "*.png"))
        if images == []:
            print("No files found found in the images directory: ",args.inputPath)
            return []
        images.sort()
    else:
        images = [args.input]
    if not os.path.exists(args.outputPath):
        os.makedirs(args.outputPath)
    for s in images:
        # NIC
        if args.model != None:
            args.target_rate = [[args.model, args.lambda_rd]]
        logging.basicConfig(filename="./test_time/EncTime.log", level=logging.INFO)
        for rate in args.target_rate:
            imagename = s.split("/")[-1]
            print(imagename, rate)

            if args.output is not None:
                # binName = args.output
                # binName = os.path.join(args.outputPath,binName)
                bin_path = os.path.join(args.outputPath, 'Enc')
                if not os.path.exists(bin_path):
                    os.makedirs(bin_path)
                binName = f'{imagename.split(".")[0]}.bin'
                # binName = args.output
                binName = os.path.join(bin_path, binName)
            else:
                dummy = imagename.split(".")[0]
                binName = os.path.join(args.outputPath,"BEE_"+dummy+"_"+f'{rate:0.02f}'+".bin")
            enc_engine = ModelEngine()
            start_time = time.time()
            enc_engine.encode(args.cfg, s, rate, args.ckptdir, binName, device)
            enc_time = time.time() - start_time
            logging.info(f'{s} {enc_time}')
            torch.cuda.empty_cache()

def main(argv):
    args = parse_args(argv[1:])
    print_args(args)
    torch.set_num_threads(1)  # just to be sure
    encode(args)

if __name__ == '__main__':
    main(sys.argv)