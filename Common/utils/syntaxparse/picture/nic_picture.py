import struct
import torch
import numpy as np
from Common.utils.nic_utils.config import dict

import os
class NIC_picture():
    def __init__(self):
        self.picture_header = NIC_picture_header()
        self.block_header = NIC_block_header()

    def write_header_to_stream():
        raise NotImplemented

    def read_header_from_stream(self, f):
        self.f = self.picture_header.read_header_from_stream(f)

    def update(self, syntax, image, rate=None):
        # imagename = image.split("/")[-1]
        # x_org, _, _ = readPngToTorchIm(image)
        # _,_,h,w = x_org.shape
        # syntax = construct_weights(syntax, rate, imagename, h, w)
        self.picture_header.read_cfg(image, rate)

class NIC_block_header():
    def __init__(self):
        self.Min_Main = None
        self.Max_Main = None
        self.Min_HYPER_1 = None
        self.Max_HYPER_1 = None
        self.Min_HYPER_2 = None
        self.Max_HYPER_2 = None
        self.FileSizeMain = None
        self.FileSizeHyper1 = None
        self.FileSizeHyper2 = None
        self.geo_index = None

    def write_header_to_stream():
        raise NotImplemented

    def read_header_from_stream(self, file_object, header, block_loc):
        bin_dir_path = os.path.dirname(file_object.name)
        print("bin_dir_path_stream: ", bin_dir_path)

        if header.USE_MULTI_HYPER:
            if header.USE_GEO:
                Block_head_len = struct.calcsize('6h3IB')
                bits = file_object.read(Block_head_len)
                [ self.Min_Main, self.Max_Main, self.Min_HYPER_1, self.Max_HYPER_1, self.Min_HYPER_2, self.Max_HYPER_2, self.FileSizeMain, self.FileSizeHyper1, self.FileSizeHyper2, self.geo_index] = struct.unpack('6h3IB', bits)
                # print(struct.unpack('6h3IB', bits))
                if self.geo_index % 2 == 0:
                    # [enc_height, enc_width] indicates shape of encoding block (after geometrical operation)
                    self.enc_height = block_loc[2]
                    self.enc_width = block_loc[3]
                else:
                    self.enc_height = block_loc[3]
                    self.enc_width = block_loc[2]
            else:
                # BUG FIXED HERE
                self.enc_height = block_loc[2]
                self.enc_width = block_loc[3]
                Block_head_len = struct.calcsize('6h3I')
                bits = file_object.read(Block_head_len)
                [ self.Min_Main, self.Max_Main, self.Min_HYPER_1, self.Max_HYPER_1, self.Min_HYPER_2, self.Max_HYPER_2, self.FileSizeMain, self.FileSizeHyper1, self.FileSizeHyper2] = struct.unpack('6h3I', bits)

            with open(f"{bin_dir_path}/main.bin", 'wb') as f:
                bits = file_object.read(self.FileSizeMain)
                f.write(bits)
            with open(f"{bin_dir_path}/hyper_1.bin", 'wb') as f:
                bits = file_object.read(self.FileSizeHyper1)
                f.write(bits)
            with open(f"{bin_dir_path}/hyper_2.bin", 'wb') as f:
                bits = file_object.read(self.FileSizeHyper2)
                f.write(bits)

        else: # Single Hyper Model
            if header.USE_GEO:
                Block_head_len = struct.calcsize('4h2IB')
                bits = file_object.read(Block_head_len)
                [self.Min_Main, self.Max_Main, self.Min_V_HYPER, self.Max_V_HYPER, self.FileSizeMain, self.FileSizeHyper,
                self.geo_index] = struct.unpack('4h2IB', bits)
                if self.geo_index % 2 == 0:
                    # [enc_height, enc_width] indicates shape of encoding block (after geometrical operation)
                    self.enc_height = block_loc[2]
                    self.enc_width = block_loc[3]
                else:
                    self.enc_height = block_loc[3]
                    self.enc_width = block_loc[2]
            else:
                Block_head_len = struct.calcsize('4h2I')
                bits = file_object.read(Block_head_len)
                [self.Min_Main, self.Max_Main, self.Min_V_HYPER, self.Max_V_HYPER, self.FileSizeMain, self.FileSizeHyper] = struct.unpack('4h2I',bits)
            with open(f"{bin_dir_path}/main.bin", 'wb') as f:
                bits = file_object.read(self.FileSizeMain)
                f.write(bits)
            with open(f"{bin_dir_path}/hyper.bin", 'wb') as f:
                bits = file_object.read(self.FileSizeHyper)
                f.write(bits)

            print("check")
        return file_object, bin_dir_path

class NIC_picture_header():
    def __init__(self):
        self.quality = 1
        self.GPU = dict['GPU']
        self.USE_SECOND_POSTPROCESSING = False
    def write_header_to_stream():
        raise NotImplemented

    def read_header_from_stream(self, file_object):
        head_len = struct.calcsize('2HB6?HB')
        bits = file_object.read(head_len)
        [self.H, self.W, self.model_index, self.USE_GEO, self.USE_VR_MODEL, self.USE_MULTI_HYPER,
            self.USE_PREDICTOR, self.USE_POST, self.USE_ACCELERATION, self.CTU_size, self.USE_SECOND_POSTPROCESSING] = struct.unpack('2HB6?HB', bits)
        #USE_SECOND_POSTPROCESSING is used to indicate whether the second postprocessing is used.
        #There this flag is default to be False.
        if self.USE_POST:
            post_ctu_len = struct.calcsize('H')
            bits = file_object.read(post_ctu_len)
            [self.POST_CTU_SIZE] = struct.unpack('H', bits)
        if self.USE_VR_MODEL:
            head_lambda_len = struct.calcsize('H')
            bits = file_object.read(head_lambda_len)
            [self.lambda_rd_nom_scaled] = struct.unpack('H', bits)
        return file_object

    def read_cfg(self, image, rate):
        self.model_index, self.lambda_rd_ori = rate[0], rate[1]
        self.GPU = dict['GPU']
        # index - [0-15]
        self.USE_VR_MODEL = dict['USE_VR_MODEL']
        if self.USE_VR_MODEL:
            models = ["mse_VR_low", "mse_VR_middle", "mse_VR_high", "msssim_VR_low", "msssim_VR_middle", "msssim_VR_high"]
            self.max_lambdas = [6, 44, 296, 0.08, 0.96, 7.68]
        else:
            models = ["mse200", "mse400", "mse800", "mse1600", "mse3200", "mse6400", "mse12800", "mse25600",
                    "msssim4", "msssim8", "msssim16", "msssim32", "msssim64", "msssim128", "msssim320", "msssim640"]
        self.USE_PREPROCESSING = dict['USE_PREPROCESSING']
        self.USE_POSTPROCESSING = dict['USE_POSTPROCESSING']

        if self.USE_PREPROCESSING:
            self.num_steps = dict['num_steps']

        if self.USE_POSTPROCESSING:
            self.post_processing_epochs = dict['Post_processing_epochs']
            self.post_processing_learning_rate = dict['Post_processing_learning_rate']
            self.postprocessing_CTU = dict['Postprocessing_CTU']

        self.USE_MULTI_HYPER = dict['USE_MULTI_HYPER']
        self.USE_PREDICTOR = dict['USE_PREDICTOR']
        self.USE_ACCELERATION = dict['USE_ACCELERATION']

        # assert (USE_MULTI_HYPER and USE_VR_MODEL) is False
        assert (self.USE_PREDICTOR and bool(1-self.USE_MULTI_HYPER)) is False

        ############################## Load Encoding Configuration Parameters ########################
        self.SAVE_REC = dict['SAVE_REC']
        self.USE_GEO = dict['USE_GEO']
        self.CTU_size = dict['CTU_size']
        self.block_width = dict['CTU_size']
        self.block_height = dict['CTU_size']

    def print_info(self):
        pass
