'''
    Contributors: Ziqing Ge
'''

import struct

############### Write a bool list into binary file ###############
############### Since the smallest unit of 'struct.pack' is Byte, ###############
############### we view 16 bools as a coding unit and struct them to 2 Bytes. ###############
def pack_bools(input_list, file):
    coding_unit_list = []
    C = len(input_list)
    if C%16>0:  #padding 0 such that the bool list can be divided into integer amounts of coding units.
        input_list += [0 for i in range(16*(C//16+1)-C)]
        C = 16*(C//16+1)
    for i in range(int(C/16)):
        coding_unit=0
        for j in range(16):
            coding_unit |= input_list[16*i+j]<<j
        coding_unit_list.append(coding_unit)
    info = struct.pack(str(int(C/16))+'H',*coding_unit_list)
    file.write(info)


############### Read a bool list from file ###############
############### C denotes the length of the reconstructed bool list. ###############
def unpack_bools(C, file):
    if C % 16 > 0:
        C = 16 * (C // 16 + 1)
    head_len = struct.calcsize(str(int(C/16))+'H')
    bits = file.read(head_len)
    coding_unit_list = struct.unpack(str(int(C/16))+'H',bits)
    input_list=[]
    for i in range(int(C/16)):
        char = coding_unit_list[i]
        for j in range(16):
            input_list.append((char&(1<<j))>>j)
    return input_list
