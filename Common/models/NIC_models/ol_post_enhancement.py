import torch.nn as nn
import torch.optim as optim
import copy
import torch
import numpy as np
import math
import random
from collections import OrderedDict
# import Common.utils.nic_utils.torch_msssim as torch_msssim


def conv3x3(in_ch, out_ch, stride=1):
    """3x3 convolution with padding."""
    return nn.Conv2d(in_ch, out_ch, kernel_size=3, stride=stride, padding=1)

def set_seed(seed):
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    np.random.seed(seed)
    random.seed(seed)

class Enhancement_net(nn.Module):
    def __init__(self):
        super(Enhancement_net, self).__init__()
        self.conv1 = conv3x3(3, 3)
    def forward(self, x):
        x1 = self.conv1(x) 
        return x1


def convert_list_to_params(net, net_ls):
    k_in = 3
    k_out = 3
    k_h = 3
    k_w = 3
    bias_part_length = 3
    weight_dict = OrderedDict()
    weight_conv_tensor = torch.zeros((k_in, k_out, k_h, k_w))
    weight_bias_tensor = torch.zeros((bias_part_length))
    conv_net = net
    conv_part_length = len(net_ls) - bias_part_length
    bias_weight = net_ls[conv_part_length:]
    conv_weight = net_ls[:conv_part_length]
    for i1 in range(k_in):
        for i2 in range(k_out):
            for i3 in range(k_h):
                for i4 in range(k_w):
                    offset = i1 * (k_out * k_h * k_w) + i2 * \
                        (k_h * k_w) + i3 * (k_w) + i4
                    weight_conv_tensor[i1][i2][i3][i4] = conv_weight[offset]
    for k in range(bias_part_length):
        weight_bias_tensor[k] = bias_weight[k]
    weight_dict['conv1.weight'] = weight_conv_tensor
    weight_dict['conv1.bias'] = weight_bias_tensor
    conv_net.load_state_dict(weight_dict)
    return conv_net


def convert_params_to_list(net):
    state_dict = net.state_dict()
    params = []
    for key, value in state_dict.items():
        if key == "conv1.weight":
            flattened = [val for sublist in value.tolist() for subsublist in sublist for subsubsublist in subsublist for val in subsubsublist]
            params.extend(flattened)
        else:
            params.extend(value.tolist())
    return params

def online_training(
    compressed_im, im, 
    block_H, block_W, 
    enhance_net, lamda, 
    online_training_epochs, online_training_learning_rate
    ):

    # online_training_learning_rate = 0.2

    isEnhance = False
    params_list = []
    #number of parameters in enhance_net
    enhance_param = 84
    enhance_net.train()

    compressed_im = compressed_im.cuda()
    compressed_im_copy = compressed_im.clone()
    im = im.cuda()

    #optimizer 
    optimizer = optim.Adam(enhance_net.parameters(), lr=online_training_learning_rate)
    scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.5, patience=500, min_lr=online_training_learning_rate / 100)
    
    #MSE distortion loss
    distortion_loss = nn.MSELoss()
    original_loss = distortion_loss(compressed_im, im)
    compression_distortion_loss = distortion_loss(compressed_im[:,:,:block_H, :block_W], im[:,:,:block_H, :block_W])

    print(f'Postprocessing: before online training, enhancement loss is: {original_loss}' )
    mini_loss = [False, original_loss]
    # --------- main loop of online training post-enhancement -----------------
    for j in range(online_training_epochs):
        if optimizer.param_groups[0]['lr'] == online_training_learning_rate / 100:
            break
        optimizer.zero_grad()
        enhance_img = enhance_net(compressed_im) 
        # enhance_img.clamp_(0, 1)
        enhance_loss = distortion_loss(enhance_img, im)
        
        # Needs a re-initialization
        if math.isnan(enhance_loss):
            #change seed to avoid the same parameters generated.
            set_seed(j)
            print("this is nan")
            enhance_net = Enhancement_net()
            enhance_net.cuda()
            optimizer = optim.Adam(enhance_net.parameters(), lr=online_training_learning_rate)
            # patience=?
            scheduler = optim.lr_scheduler.ReduceLROnPlateau(
                optimizer, factor=0.5, patience=500, min_lr=online_training_learning_rate / 100)
            continue
        set_seed(0)

        enhance_loss.backward()
        optimizer.step()
        scheduler.step(enhance_loss)

        if enhance_loss < mini_loss[1]:
            enhance_net_copy = copy.deepcopy(enhance_net)
            mini_loss = [enhance_net_copy, enhance_loss]

        # print log file
        # if j%100 == 0:
        #     lr = optimizer.param_groups[0]['lr']
        #     print(f'at epoch: {str(j)} enhancement loss is: {enhance_loss}, and lr is: {lr}' )

    # --------- calculate the actuall loss and compare the one without postprocessing-----------------
    trained_enhance_net, enhance_loss = mini_loss[0], mini_loss[1]
    if trained_enhance_net:
        print(f'best enhancement loss is: {enhance_loss}')

        #16 bit compression
        trained_enhance_net.eval()
        trained_enhance_net.half()
        trained_enhance_net_in_half = copy.deepcopy(trained_enhance_net)
        trained_enhance_net.float()
        enhance_img16 = trained_enhance_net(compressed_im)
        enhance_img16 = enhance_img16.type(torch.cuda.FloatTensor)

        # if the MSE loss is lower (trained_enhance_net is True), check RD loss
        enhanced16_distortion_loss = distortion_loss(enhance_img16[:,:,:block_H, :block_W], im[:,:,:block_H, :block_W])
        print(f'visual improvement is {(compression_distortion_loss - enhanced16_distortion_loss)/compression_distortion_loss}')
        # added
        # if reconstruct_metric == 'mse':
        block_loss = lamda * (255 ** 2) * compression_distortion_loss
        enhance_loss16 = lamda * (255 ** 2) * enhanced16_distortion_loss + enhance_param * 16 / (block_H * block_W)
        # else:
        #     block_loss = lamda * compression_distortion_loss
        #     enhance_loss16 = lamda * enhanced16_distortion_loss + enhance_param * 16 / (block_H * block_W)

        #RD check
        if block_loss <= enhance_loss16:
            # even the enhanced img with lower MSE, the RD loss still higher
            print(f"The block is not selected!!!")
        else:
            improvement = (block_loss - enhance_loss16)/block_loss
            print(f"The block is selected based on RD selection in 16 bit, and the improvement is: {improvement}")
            isEnhance = True
            compressed_im_copy = enhance_img16
            params_list = convert_params_to_list(trained_enhance_net_in_half)

    return isEnhance, params_list, compressed_im_copy

