import sys

import cv2
import struct
import numpy as np
import torch
import torch.nn as nn
from PIL import Image
from os.path import abspath, join, dirname, pardir
from Common.utils.nic_utils.config import dict
from Common.models.NIC_models.ol_post_enhancement import convert_list_to_params, Enhancement_net, online_training
sys.path.append(abspath(dirname(__file__)))
from RestormerPostprocessing import Restormer

class NIC_Postprocess(nn.Module):
    def __init__(self):
        super().__init__()
        self.enhance_net = Enhancement_net()

    def encode(self, source_img, out_img, file_object, header, rec_dir):
        #############################################    postprocessing     ###################################################################
        #post-processing only performs when reconstruction_metrics is MSE
        H, W, C = header.H, header.W, header.C
        if header.USE_POSTPROCESSING and header.reconstruction_metric == "mse":
            out_img_pp = np.zeros([H, W, C])  # recon image added
            W_offset_pp = 0
            H_offset_pp = 0
            ############

            enhance_net = Enhancement_net()
            source_img_normalized = source_img / 255.0
            out_img_normalized = out_img[:H, :W, :]
            ######################### Spliting Image #########################
            block_height_pp = header.postprocessing_CTU
            block_width_pp = header.postprocessing_CTU
            Block_Num_in_Width_pp = int(np.ceil(W / block_width_pp))
            Block_Num_in_Height_pp = int(np.ceil(H / block_height_pp))
            img_block_list_pp = []
            out_img_block_list_pp = []
            for i in range(Block_Num_in_Height_pp):
                for j in range(Block_Num_in_Width_pp):
                    img_block_list_pp.append(source_img_normalized[i * block_height_pp:np.minimum((i + 1) * block_height_pp, H),j * block_width_pp:np.minimum((j + 1) * block_width_pp,W),...])
                    out_img_block_list_pp.append(out_img_normalized[i * block_height_pp:np.minimum((i + 1) * block_height_pp, H),j * block_width_pp:np.minimum((j + 1) * block_width_pp,W),...])
            ######################### Padding Image #########################
            Block_Idx_pp = 0
            for idx, img_pp in enumerate(img_block_list_pp): # Traverse CTUs

                block_H_pp, block_W_pp, _ = img_pp.shape
                im_pp = torch.FloatTensor(img_pp)
                im_pp = im_pp.permute(2, 0, 1).contiguous()
                im_pp = im_pp.view(1, C, block_H_pp, block_W_pp)

                out_img_block_pp = out_img_block_list_pp[idx]
                out_im_pp = torch.FloatTensor(out_img_block_pp)
                out_im_pp = out_im_pp.permute(2, 0, 1).contiguous()
                out_im_pp = out_im_pp.view(1, C, block_H_pp, block_W_pp)

                if header.GPU:
                    im_pp = im_pp.cuda()
                    if header.USE_VR_MODEL:
                        lambda_rd = header.lambda_rd.cuda()
                # print('====> Post processing Image:', im_dir, "%dx%d" % (block_H_pp, block_W_pp), 'to', out_dir, " Block Idx: %d" % (Block_Idx_pp))
                Block_Idx_pp += 1

                #################################### Content adaptive Online Training (post processing) ########################
                #only perform the postprocessing if the optimization metric is MSE
                if block_H_pp >= 3 and block_W_pp >= 3:
                    enhance_net = Enhancement_net()
                    enhance_net.cuda()
                    isEnhance, params_list, rec_img_block = online_training(out_im_pp, im_pp,
                                                                            block_H_pp, block_W_pp,
                                                                            enhance_net, header.lmbda,
                                                                            header.post_processing_epochs, header.post_processing_learning_rate)  ## 保存重建图片

                    Head_pp_block = struct.pack('?', isEnhance)
                    file_object.write(Head_pp_block)  # CU information

                    if isEnhance:
                        pp_block_params = struct.pack('84e', *params_list)
                        file_object.write(pp_block_params)

                ## added by sxd
                ################################### Reconstruct Image #######################################
                output_ = torch.clamp(rec_img_block, min=0., max=1.0)

                out = output_.data[0].cpu().numpy()
                out = out.transpose(1, 2, 0)

                out_img_pp[H_offset_pp: H_offset_pp + block_H_pp, W_offset_pp: W_offset_pp + block_W_pp,
                :] = out[:block_H_pp, :block_W_pp, :]

                W_offset_pp += block_W_pp
                if W_offset_pp >= W:
                    W_offset_pp = 0
                    H_offset_pp += block_H_pp
            out_img = out_img_pp
            ##########

        file_object.close()
        ## added save image by sxd
        out_img = np.round(out_img * 255.0)
        out_img = out_img.astype('uint8')
        out_img = out_img[:H, :W, :]
        img = Image.fromarray(out_img)
        img.save(rec_dir)

    def decode(self, imArray, file_object, header, recon_path):
        H, W = header.H, header.W
        if header.USE_POST:
            H, W, POST_CTU_SIZE = header.H, header.W, header.POST_CTU_SIZE
            W_offset_pp = 0
            H_offset_pp = 0
            enhance_net = Enhancement_net()

            out_img_pp = np.zeros([H, W, 3]) # recon image
            out_img_normalized = imArray[:H, :W, :]
            ######################### Spliting Image #########################
            Block_Num_in_Width_pp = int(np.ceil(W / POST_CTU_SIZE))
            Block_Num_in_Height_pp = int(np.ceil(H / POST_CTU_SIZE))

            for i in range(Block_Num_in_Height_pp):
                for j in range(Block_Num_in_Width_pp):
                    pp_block = out_img_normalized[i * POST_CTU_SIZE:np.minimum((i + 1) * POST_CTU_SIZE, H),j * POST_CTU_SIZE:np.minimum((j + 1) * POST_CTU_SIZE,W),...]
                    block_H_pp, block_W_pp, C = pp_block.shape
                    im_pp = torch.FloatTensor(pp_block)
                    im_pp = im_pp.permute(2, 0, 1).contiguous()
                    im_pp = im_pp.view(1, C, block_H_pp, block_W_pp)

                    if block_H_pp >= 3 and block_W_pp >= 3:
                        is_enhance_len = struct.calcsize('?')
                        bits = file_object.read(is_enhance_len)
                        [is_enhance] = struct.unpack('?', bits)

                        if is_enhance:
                            params_list_len = struct.calcsize('84e')
                            bits = file_object.read(params_list_len)
                            params_list = struct.unpack('84e', bits)

                            enhance_net = convert_list_to_params(enhance_net, params_list)
                            if dict['GPU']:
                                enhance_net = enhance_net.cuda()
                                im_pp = im_pp.cuda()
                            output_ = enhance_net(im_pp)
                        else:
                            output_ = im_pp
                    else:
                        output_ = im_pp

                    output_ = torch.clamp(output_, min=0., max=1.0)

                    out = output_.data[0].cpu().numpy()
                    out = out.transpose(1, 2, 0)

                    out_img_pp[H_offset_pp : H_offset_pp + POST_CTU_SIZE, W_offset_pp : W_offset_pp + POST_CTU_SIZE, :] = out[:POST_CTU_SIZE, :POST_CTU_SIZE, :]

                    W_offset_pp += POST_CTU_SIZE
                    if W_offset_pp >= W:
                        W_offset_pp = 0
                        H_offset_pp += POST_CTU_SIZE
                out_img = out_img_pp

        if header.USE_SECOND_POSTPROCESSING:
            restormer = Restormer()
            #load the model
            #restormer.load_state_dict(torch.load('restormer.pth'))
            if dict['GPU']:
                restormer = restormer.cuda()

            for h in range(0, H, POST_CTU_SIZE):
                for w in range(0, W, POST_CTU_SIZE):
                    block = out_img[h:h+POST_CTU_SIZE, w:w+POST_CTU_SIZE, :]
                    block = torch.from_numpy(block).permute(2, 0, 1).unsqueeze(0).float()
                    if dict['GPU']:
                        block = block.cuda()

                    with torch.no_grad():
                        out_block = restormer(block)
                    out_block = out_block.cpu().numpy().squeeze().transpose(1, 2, 0)
                    out_img[h:h+POST_CTU_SIZE, w:w+POST_CTU_SIZE, :] = out_block

        out_img = np.round(out_img * 255.0)
        out_img = out_img.astype('uint8')
        img = Image.fromarray(out_img[:H, :W, :])
        img.save(recon_path)
        return 0

    def writePngOutput(pytorchIm,bitDepth,bitshift, outFileName):
        output = outFileName
        if output is not None:
            img = pytorchIm.squeeze().permute(1, 2, 0).cpu().numpy()
            img = (img*((1<<bitDepth) - 1)).round()
            img = img.clip(0, (1<<bitDepth)-1)
            img *=  (1<<bitshift)
            img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
            if bitDepth <= 8:
                cv2.imwrite(output, img.astype(np.uint8))
            else:
                cv2.imwrite(output, img.astype(np.uint16))
        else:
            print("not writing output.")

