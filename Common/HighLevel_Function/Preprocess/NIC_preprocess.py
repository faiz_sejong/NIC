import struct
import numpy as np
import torch
import torch.nn as nn
from PIL import Image
from Common.utils.nic_utils.block_metric import check_RD_GEO # block based metric
from Common.utils.nic_utils.generate_substitute import SubstituteGenerator
import Common.models.NIC_models.model as model
from Common.models.NIC_models.context_model import Weighted_Gaussian, Weighted_Gaussian_res_2mask, Multistage

lambdas_list = [0.00105, 0.0042, 0.0160, 0.0618, 0.2950,  ##mse
                1.40, 6.50, 27.00, 115.37, 440.00]  ##ms-ssim


class NIC_Preprocess(nn.Module):
    def __init__(self, header):
        super().__init__()
        model_index, lambda_rd_ori = header.model_index, header.lambda_rd_ori

        if header.USE_VR_MODEL:
            header.reconstruction_metric = 'mse' if model_index <= 2 else 'msssim'
            header.lmbda = lambda_rd_ori * 100
            lambda_rd_max = header.max_lambdas[model_index]
            if lambda_rd_ori > 1.2 * lambda_rd_max:
                lambda_rd_ori = 1.2 * lambda_rd_max
            lambda_rd_nom = lambda_rd_ori / lambda_rd_max
            header.lambda_rd_nom_scaled = int(lambda_rd_nom / 1.2 * pow(2, 16))
            lambda_rd_nom_used = header.lambda_rd_nom_scaled / pow(2, 16) * 1.2
            lambda_rd_numpy = np.zeros((1, 1), np.float32)
            lambda_rd_numpy[0, 0] = lambda_rd_nom_used
            header.lambda_rd = torch.Tensor(lambda_rd_numpy)
            M, N2 = 192, 128
            if (model_index == 2) or (model_index == 5):
                M, N2 = 256, 192
            if header.USE_MULTI_HYPER:
                if header.USE_PREDICTOR:
                    self.image_comp = model.Image_coding_multi_hyper_res(3, M, N2, M, M // 2)
                else:
                    self.image_comp = model.Image_coding_multi_hyper(3, M, N2, M, M // 2)
            else:
                self.image_comp = model.Image_coding(3, M, N2, M, M // 2)
            if header.USE_PREDICTOR:
                self.context = Multistage(M)
            else:
                self.context = Weighted_Gaussian(M)
        else:
            header.reconstruction_metric = 'mse' if model_index <= 4 else 'msssim'
            header.lmbda = lambdas_list[model_index]
            M, N2 = 192, 128
            # if (model_index == 6) or (model_index == 7) or (model_index == 14) or (model_index == 15):
            #     M, N2 = 256, 192
            if header.USE_MULTI_HYPER:
                if header.USE_PREDICTOR:
                    self.image_comp = model.Image_coding_multi_hyper_res(3, M, N2, M, M // 2)
                else:
                    self.image_comp = model.Image_coding_multi_hyper(3, M, N2, M, M // 2)
            else:
                self.image_comp = model.Image_coding(3, M, N2, M, M // 2)
            if header.USE_PREDICTOR:
                self.context = Multistage(M)
            else:
                self.context = Weighted_Gaussian(M)
            header.lambda_rd = None

        if header.USE_PREPROCESSING:
            # lmbda_list = [200, 400, 800, 1600, 3200, 6400, 12800, 25600, 4, 8, 16, 32, 64, 128, 320, 640]
            # stepsize_list = [150, 75, 30, 10, 10, 5, 3, 1, 100, 10, 7, 5, 1, 1, 1, 0.3]
            #Todo: This part is different from NIC-v0.5
            stepsize_list = [150, 75, 30, 10, 10, 100, 10, 7, 5, 1]

            if header.USE_VR_MODEL:
                # header.reconstruction_metric = 'mse' if model_index <= 2 else 'msssim'
                # header.lmbda = lambda_rd_ori * 100
                step_size = self.get_step(header.lmbda, header.reconstruction_metric)
                print(step_size)
            else:
                step_size = stepsize_list[model_index]
                # header.lmbda = lmbda_list[model_index]
                # header.reconstruction_metric = 'mse' if model_index <= 7 else 'msssim'

            self.substitute_generator = SubstituteGenerator(model=self.image_comp, context_model=self.context, llambda=header.lmbda,
                                           num_steps=header.num_steps, step_size=step_size,
                                           reconstruct_metric=header.reconstruction_metric,
                                           )

    @staticmethod
    def get_step(lamb, metric):
        if metric == 'mse':
            lmbda_list = [40, 200, 400, 800, 1600, 3200, 6400, 12800, 25600, 29600]
            stepsize_list = [200, 150, 75, 30, 10, 10, 5, 3, 1, 1]
        elif metric == 'msssim':
            lmbda_list = [0.5, 4, 8, 16, 32, 64, 128, 320, 640, 768]
            stepsize_list = [150, 100, 10, 7, 5, 1, 1, 1, 0.3, 0.3]
        else:
            pass

        floor = np.array(lmbda_list)[:-1].astype(np.float32)
        floor[floor>lamb] = 0
        ceil = np.array(lmbda_list)[1:].astype(np.float32)
        ceil[ceil<lamb] = 0
        index = np.nonzero(floor*ceil)[0][0]
        range = lmbda_list[index+1] - lmbda_list[index]
        alpha1 = (lamb - lmbda_list[index])/range
        alpha2 = (lmbda_list[index+1] - lamb)/range
        return alpha1*stepsize_list[index+1]+alpha2*stepsize_list[index]

    def encode(self, image, file_object, picture, device):
        header, block_header = picture.picture_header, picture.block_header
        img = Image.open(image)
        source_img = np.array(img)
        H, W, C_ = source_img.shape
        if C_ > 3:
            source_img = source_img[:,:,:3]
            print("[WARNING]: Input image has 4 channels! only first 3 channels are encoded.")
        C = 3
        img = source_img / 255.0
        num_pixels = H * W
        # block_header.block_width = header.CTU_size
        # block_header.block_height = header.CTU_size
        block_header.H_offset = 0
        block_header.W_offset = 0

        header.Block_Num_in_Width = int(np.ceil(W / header.CTU_size))
        header.Block_Num_in_Height = int(np.ceil(H / header.CTU_size))

        # write head info to file
        if header.reconstruction_metric == 'msssim':
            Head = struct.pack('2HB6?H', H, W, header.model_index, header.USE_GEO, header.USE_VR_MODEL, header.USE_MULTI_HYPER, header.USE_PREDICTOR,
                           False, header.USE_ACCELERATION, header.CTU_size)
        else:
            Head = struct.pack('2HB6?HB', H, W, header.model_index, header.USE_GEO, header.USE_VR_MODEL, header.USE_MULTI_HYPER, header.USE_PREDICTOR, header.USE_POSTPROCESSING, header.USE_ACCELERATION, header.CTU_size, header.USE_SECOND_POSTPROCESSING)
        file_object.write(Head)

        if header.USE_POSTPROCESSING and header.reconstruction_metric == 'mse':
            Head_post_CTU = struct.pack('H', header.postprocessing_CTU)
            file_object.write(Head_post_CTU)

        if header.USE_VR_MODEL:
            Head_lmbda = struct.pack('H', header.lambda_rd_nom_scaled)
            file_object.write(Head_lmbda)

        ######################### spliting Image #########################
        Block_Num_in_Width = int(np.ceil(W / header.block_width))
        Block_Num_in_Height = int(np.ceil(H / header.block_height))
        img_block_list = []
        for i in range(Block_Num_in_Height):
            for j in range(Block_Num_in_Width):
                img_block_list.append(img[i * header.block_height:np.minimum((i + 1) * header.block_height, H),
                                    j * header.block_width:np.minimum((j + 1) * header.block_width, W), ...])
        print('check')
        header.H, header.W, header.C = H, W, C
        return file_object, source_img, img_block_list

    def process_block(self, im_block, header):
        ############################ Geometric Flip and rotate ########################
        if header.USE_GEO:
            _, _, header.geo_index, _, header.orig_rd = check_RD_GEO(im_block, header.lambda_rd, self.image_comp, self.context, header.model_index)
            # TODO: header.block.i_rot
            header.i_rot = int(header.geo_index % 4)
            if header.geo_index < 4:
                im_block = torch.rot90(im_block, k=header.i_rot, dims=[2, 3])
            else:
                im_block = torch.rot90(torch.flip(im_block, dims=[2]), k=header.i_rot, dims=[2, 3])

        ############################ Preprocessing to find a substitute ########################
        if header.USE_PREPROCESSING:
            if header.USE_VR_MODEL:
                im_block = self.substitute_generator.perturb(orig_image=im_block, orig_rd = header.orig_rd, lambda_rd=header.lambda_rd)
            else:
                im_block = self.substitute_generator.perturb(orig_image=im_block, orig_rd = header.orig_rd,)
        return im_block

