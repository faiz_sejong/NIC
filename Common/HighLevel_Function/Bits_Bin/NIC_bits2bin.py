import struct
import time
import torch
import torch.nn as nn
import numpy as np
# from Common import available_entropy_coders
# from Common.models.ans import RansDecoder
# from Common import set_entropy_coder
from Common.models.NIC_models.basic_module import P_NN
from Common.models.NIC_models.factorized_entropy_model import Entropy_bottleneck
from Common.models.NIC_models.hyper_module import h_synthesisTransform
from Common.models.NIC_models.context_model import Weighted_Gaussian, Weighted_Gaussian_res_2mask, P_Model
from Common.utils.nic_utils.config import dict
import AE


class NIC_Bits2Bin(nn.Module):
    def __init__(self, header, **kwargs):
        super().__init__()
        device = 'cpu' if kwargs.get("device") == None else kwargs.get("device")
        H, W, CTU_size, model_index = header.H, header.W, header.CTU_size, header.model_index
        header.block_width = CTU_size
        header.block_height = CTU_size
        C = 3

        H_offset = 0
        W_offset = 0
        header.Block_Num_in_Width = int(np.ceil(W / header.block_width))
        header.Block_Num_in_Height = int(np.ceil(H / header.block_height))

        if header.USE_VR_MODEL:
            lambda_rd_nom_used = header.lambda_rd_nom_scaled / pow(2, 16) * 1.2
            # print(lambda_rd_nom_used)
            lambda_rd_numpy = np.zeros((1, 1), np.float32)
            lambda_rd_numpy[0, 0] = lambda_rd_nom_used
            header.lambda_rd = torch.Tensor(lambda_rd_numpy)
            M, N2 = 192, 128
            if (model_index == 2) or (model_index == 5):
                M, N2 = 256, 192
            if header.USE_MULTI_HYPER:
                if header.USE_PREDICTOR:
                    # self.image_comp = model.Image_coding_multi_hyper_res(3, M, N2, M, M // 2)
                    self.factorized_entropy_func = Entropy_bottleneck(128)
                    if M == 192:
                        self.hyper_1_dec = h_synthesisTransform(256, [768, 768, 768, M], [1, 1, 1])
                        self.hyper_2_dec = h_synthesisTransform(128, [64 * 4, 64 * 4, 64 * 4, 64 * 4], [1, 1, 1])
                    elif M == 256:
                        self.hyper_1_dec = h_synthesisTransform(256, [768 * 2, 768 * 2, 768 * 2, M], [1, 1, 1])
                        self.hyper_2_dec = h_synthesisTransform(128, [64 * 4 * 2, 64 * 4 * 2, 64 * 4 * 2, 64 * 4], [1, 1, 1])
                    self.p_2 = P_Model(256)
                    self.Y_2 = P_NN(256, 2)
                else:
                    # self.image_comp = model.Image_coding_multi_hyper(3, M, N2, M, M // 2)
                    self.factorized_entropy_func = Entropy_bottleneck(128)
                    if M == 192:
                        self.hyper_1_dec = h_synthesisTransform(256, [768, 768, 768, M], [1, 1, 1])
                        self.hyper_2_dec = h_synthesisTransform(128, [64 * 4, 64 * 4, 64 * 4, 64 * 4], [1, 1, 1])
                    elif M == 256:
                        self.hyper_1_dec = h_synthesisTransform(256, [768 * 2, 768 * 2, 768 * 2, M], [1, 1, 1])
                        self.hyper_2_dec = h_synthesisTransform(128, [64 * 4 * 2, 64 * 4 * 2, 64 * 4 * 2, 64 * 4], [1, 1, 1])
                    self.p_2 = P_Model(256)
            else:
                # self.image_comp = model.Image_coding(3, M, N2, M, M // 2)
                self.factorized_entropy_func = Entropy_bottleneck(N2)
        else:
            M, N2 = 192, 128
            # if (model_index == 6) or (model_index == 7) or (model_index == 14) or (model_index == 15):
            #     M, N2 = 256, 192
            if header.USE_MULTI_HYPER:
                if header.USE_PREDICTOR:
                    # self.image_comp = model.Image_coding_multi_hyper_res(3, M, N2, M, M // 2)
                    self.factorized_entropy_func = Entropy_bottleneck(128)
                    if M == 192:
                        self.hyper_1_dec = h_synthesisTransform(256, [768, 768, 768, M], [1, 1, 1])
                        self.hyper_2_dec = h_synthesisTransform(128, [64 * 4, 64 * 4, 64 * 4, 64 * 4], [1, 1, 1])
                    elif M == 256:
                        self.hyper_1_dec = h_synthesisTransform(256, [768 * 2, 768 * 2, 768 * 2, M], [1, 1, 1])
                        self.hyper_2_dec = h_synthesisTransform(128, [64 * 4 * 2, 64 * 4 * 2, 64 * 4 * 2, 64 * 4], [1, 1, 1])
                    self.p_2 = P_Model(256)
                    self.Y_2 = P_NN(256, 2)
                else:
                    # self.image_comp = model.Image_coding_multi_hyper(3, M, N2, M, M // 2)
                    self.factorized_entropy_func = Entropy_bottleneck(128)
                    if M == 192:
                        self.hyper_1_dec = h_synthesisTransform(256, [768, 768, 768, M], [1, 1, 1])
                        self.hyper_2_dec = h_synthesisTransform(128, [64 * 4, 64 * 4, 64 * 4, 64 * 4], [1, 1, 1])
                    elif M == 256:
                        self.hyper_1_dec = h_synthesisTransform(256, [768 * 2, 768 * 2, 768 * 2, M], [1, 1, 1])
                        self.hyper_2_dec = h_synthesisTransform(128, [64 * 4 * 2, 64 * 4 * 2, 64 * 4 * 2, 64 * 4], [1, 1, 1])
                    self.p_2 = P_Model(256)
            else:
                # self.image_comp = model.Image_coding(3, M, N2, M, M // 2)
                self.factorized_entropy_func = Entropy_bottleneck(N2)
            header.lambda_rd = None

        self.c_main = M
        if header.USE_MULTI_HYPER:
            self.c_hyper = 256
            self.c_hyper_2 = 128
        else:
            self.c_hyper = N2

    def update(self,header):
        raise NotImplementedError

    def decode(self, file_object, block_loc, header, block_header):
        precise = 16
        file_object, bin_dir_path = block_header.read_header_from_stream(file_object, header, block_loc)
        ## added
        # bin_dir_path = os.path.dirname(file_object.name)
        print("check bin_dir_path: ", bin_dir_path)

        if header.USE_MULTI_HYPER:
            ############### Hyper 2 Decoder ###############
            # [Min_V - 0.5 , Max_V + 0.5]
            sample = np.arange(block_header.Min_HYPER_2, block_header.Max_HYPER_2+1+1)
            sample = np.tile(sample, [self.c_hyper_2, 1, 1])
            # Here goes HYY
            lower = torch.sigmoid(self.factorized_entropy_func._logits_cumulative(
                torch.FloatTensor(sample).cuda() - 0.5, stop_gradient=False))
            cdf_h = lower.data.cpu().numpy()*((1 << precise) - (block_header.Max_HYPER_2 -
                                                                block_header.Min_HYPER_2 + 1))  # [N1, 1, Max - Min]
            cdf_h = cdf_h.astype(np.int) + sample.astype(np.int) - block_header.Min_HYPER_2
            T2 = time.time()

            AE.init_decoder(f"{bin_dir_path}/hyper_2.bin", block_header.Min_HYPER_2, block_header.Max_HYPER_2)

            Recons = []
            for ii in range(self.c_hyper_2):
                for jj in range(int(block_header.enc_height * block_header.enc_width / 64 / 64)):
                    #print(cdf_h[i,0,:])
                    Recons.append(AE.decode_cdf(cdf_h[ii, 0, :].tolist()))

            # reshape Recons to y_hyper_q   [1, c_hyper, H_PAD/64, W_PAD/64]
            # correct H_PAD/W_PAD to enc_height/enc_width. by gzq
            y_hyper_2_q = torch.reshape(torch.Tensor(
                Recons), [1, self.c_hyper_2, int(block_header.enc_height / 64), int(block_header.enc_width / 64)])


            #IPython.embed()
            ############### Hyper 1 Decoder ###############
            # hyper_dec = image_comp.p(image_comp.hyper_dec(y_hyper_q))
            tmp2 = self.hyper_2_dec(y_hyper_2_q.cuda())
            hyper_2_dec = self.p_2(tmp2)
            # print("hyper_2_dec",hyper_2_dec.mean())
            _, c, h, w = hyper_2_dec.shape
            c //= 2
            mean = hyper_2_dec[:, :c, :, :]
            scale = hyper_2_dec[:, c:, :, :]
            scale = torch.abs(scale)
            scale[scale < 1e-6] = 1e-6
            #import IPython
            #IPython.embed()
            m = torch.distributions.normal.Normal(mean, scale)

            sample = np.arange(block_header.Min_HYPER_1, block_header.Max_HYPER_1+1+1)  # [Min_V - 0.5 , Max_V + 0.5]
            sample = torch.FloatTensor(np.tile(sample, [1, c, h, w, 1])).cuda()

            lower = torch.zeros(1, c, h, w, block_header.Max_HYPER_1-block_header.Min_HYPER_1+2).cuda()
            for cc in range(sample.shape[-1]):
                lower[...,cc] = m.cdf(sample[...,cc] - 0.5)
            # lower = m.cdf(sample-0.5)


            cdf_m = lower.data.cpu().numpy()*((1 << precise) - (block_header.Max_HYPER_1 - block_header.Min_HYPER_1 + 1))
            cdf_m = cdf_m.astype(np.int32) + sample.cpu().numpy().astype(np.int32) - block_header.Min_HYPER_1

            AE.init_decoder(f"{bin_dir_path}/hyper_1.bin", block_header.Min_HYPER_1, block_header.Max_HYPER_1)
            Recons = []
            for ii in range(c):
                for jj in range(int(h)):
                    for kk in range(int(w)):
                        #import IPython
                        #IPython.embed()
                        #print(ii,jj,kk)
                        Recons.append(AE.decode_cdf(cdf_m[0, ii, jj, kk, :].tolist()))

            y_hyper_q = torch.reshape(torch.Tensor(Recons), [1, c, h, w]).cuda()
            if header.USE_PREDICTOR:
                y_hyper_q = y_hyper_q + self.Y_2(tmp2)

        else: # Single Hyper Model
            ############### Hyper Decoder ###############
            # [Min_V - 0.5 , Max_V + 0.5]
            sample = np.arange(block_header.Min_V_HYPER, block_header.Max_V_HYPER + 1 + 1)
            print("check2")
            sample = np.tile(sample, [self.c_hyper, 1, 1])
            sample_tensor = torch.FloatTensor(sample)
            if header.GPU:
                sample_tensor = sample_tensor.cuda()
            lower = torch.sigmoid(self.factorized_entropy_func._logits_cumulative(
                sample_tensor - 0.5, stop_gradient=False))
            print("check2")
            cdf_h = lower.data.cpu().numpy() * ((1 << precise) - (block_header.Max_V_HYPER -
                                                                block_header.Min_V_HYPER + 1))  # [N1, 1, Max - Min]
            cdf_h = cdf_h.astype(np.int) + sample.astype(np.int) - block_header.Min_V_HYPER
            T2 = time.time()
            print("check2")
            AE.init_decoder(f"{bin_dir_path}/hyper.bin", block_header.Min_V_HYPER, block_header.Max_V_HYPER)
            print("check2")
            Recons = []
            for i in range(self.c_hyper):
                for j in range(int(block_header.enc_height * block_header.enc_width / 64 / 64)):
                    # print(cdf_h[i,0,:])
                    Recons.append(AE.decode_cdf(cdf_h[i, 0, :].tolist()))
            # reshape Recons to y_hyper_q   [1, c_hyper, H_PAD/64, W_PAD/64]
            print("check2")
            y_hyper_q = torch.reshape(torch.Tensor(
                Recons), [1, self.c_hyper, int(block_header.enc_height / 64), int(block_header.enc_width / 64)])
            print("check2")
        block_header.c_main = self.c_main
        return y_hyper_q, block_header, bin_dir_path