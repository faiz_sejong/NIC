import os
import struct
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import AE
from Common.models.NIC_models.acceleration_utils import pack_bools
from Common.models.NIC_models.context_model import Demultiplexerv2
from Common.models.NIC_models.factorized_entropy_model import Entropy_bottleneck
from Common.models.NIC_models.hyper_module import h_synthesisTransform


class NIC_Bin2Bits(nn.Module):
    def __init__(self, header, **kwargs):
        super().__init__()
        device = 'cpu' if kwargs.get("device") == None else kwargs.get("device")
        model_index = header.model_index

        if header.USE_VR_MODEL:
            lambda_rd_nom_used = header.lambda_rd_nom_scaled / pow(2, 16) * 1.2
            # print(lambda_rd_nom_used)
            lambda_rd_numpy = np.zeros((1, 1), np.float32)
            lambda_rd_numpy[0, 0] = lambda_rd_nom_used
            header.lambda_rd = torch.Tensor(lambda_rd_numpy)
            M, N2 = 192, 128
            if (model_index == 2) or (model_index == 5):
                M, N2 = 256, 192
            if header.USE_MULTI_HYPER:
                if header.USE_PREDICTOR:
                    self.factorized_entropy_func = Entropy_bottleneck(128)
            else:
                # self.image_comp = model.Image_coding(3, M, N2, M, M // 2)
                self.factorized_entropy_func = Entropy_bottleneck(N2)
        else:
            M, N2 = 192, 128
            # if (model_index == 6) or (model_index == 7) or (model_index == 14) or (model_index == 15):
            #     M, N2 = 256, 192
            if header.USE_MULTI_HYPER:
                self.factorized_entropy_func = Entropy_bottleneck(128)
            else:
                # self.image_comp = model.Image_coding(3, M, N2, M, M // 2)
                self.factorized_entropy_func = Entropy_bottleneck(N2)
            header.lambda_rd = None

        self.c_main = M
        if header.USE_MULTI_HYPER:
            self.c_hyper = 256
            self.c_hyper_2 = 128
        else:
            self.c_hyper = N2

    def encode(self, bits2bin_varlist, header, file_object, Block_Idx):
        ## added extra dirs for temporary bin, then delete
        bin_dir_path = os.path.dirname(file_object.name)  ## bin 所在目录
        print("bin_dir_path: ", bin_dir_path)

        # Main Arith Encode
        y_main_q, y_hyper_q, params_prob = bits2bin_varlist[0], bits2bin_varlist[1], bits2bin_varlist[2]
        channel_num = y_main_q.shape[1]
        y_main_q = torch.cat((torch.cat(Demultiplexerv2(y_main_q[:, :channel_num // 2, :, :]), dim=1),
                             torch.cat(Demultiplexerv2(y_main_q[:, channel_num // 2:, :, :]), dim=1)), dim=1)
        #
        Datas = torch.reshape(y_main_q, [-1]).cpu().numpy().astype(np.int).tolist()
        _, c, h, w = y_main_q.shape
        if header.USE_ACCELERATION:
            EC_this_block = []
            Effective_Datas = []
            EC_last_block = []
            for i in range(c):
                current_channel = Datas[i * h * w:(i + 1) * h * w]
                if max(current_channel) == 0 and min(current_channel) == 0:
                    EC_this_block.append(0)
                else:
                    EC_this_block.append(1)
                    Effective_Datas += current_channel
            if Block_Idx == 1:
                pack_bools(EC_this_block, file_object)
            else:
                if EC_this_block == EC_last_block:
                    b = struct.pack('1?', 1)
                    file_object.write(b)
                else:
                    b = struct.pack('1?', 0)
                    file_object.write(b)
                    pack_bools(EC_this_block, file_object)
            EC_last_block = EC_this_block
            EC_index = [i for i in range(c) if EC_this_block[i] == 1]
            EC_index = torch.LongTensor(EC_index).cuda()
            params_prob = torch.index_select(params_prob, dim=2, index=EC_index)

        Max_Main = max(Datas)
        Min_Main = min(Datas)
        sample = np.arange(Min_Main, Max_Main+1+1)  # [Min_V - 0.5 , Max_V + 0.5]
        print("Main Channel:", c)
        if header.USE_ACCELERATION:
            sample = torch.FloatTensor(np.tile(sample, [1, len(EC_index), h, w, 1])).cuda()
        else:
            sample = torch.FloatTensor(np.tile(sample, [1, c, h, w, 1])).cuda()

        # 3 gaussian
        prob0, mean0, scale0, prob1, mean1, scale1, prob2, mean2, scale2 = [
            torch.chunk(params_prob, 9, dim=1)[i].squeeze(1) for i in range(9)]
        del params_prob
        # keep the weight summation of prob == 1
        probs = torch.stack([prob0, prob1, prob2], dim=-1)
        del prob0, prob1, prob2

        probs = F.softmax(probs, dim=-1)
        # process the scale value to positive non-zero
        scale0 = torch.abs(scale0)
        scale1 = torch.abs(scale1)
        scale2 = torch.abs(scale2)
        scale0[scale0 < 1e-6] = 1e-6
        scale1[scale1 < 1e-6] = 1e-6
        scale2[scale2 < 1e-6] = 1e-6
        m0 = torch.distributions.normal.Normal(mean0, scale0)
        m1 = torch.distributions.normal.Normal(mean1, scale1)
        m2 = torch.distributions.normal.Normal(mean2, scale2)
        if header.USE_ACCELERATION:
            lower = torch.zeros(1, len(EC_index), h, w, Max_Main - Min_Main + 2)
        else:
            lower = torch.zeros(1, c, h, w, Max_Main-Min_Main+2)

        for i in range(sample.shape[4]):
            # print("CDF:", i)
            lower0 = m0.cdf(sample[:, :, :, :, i].cuda()-0.5)
            lower1 = m1.cdf(sample[:, :, :, :, i].cuda()-0.5)
            lower2 = m2.cdf(sample[:, :, :, :, i].cuda()-0.5)
            lower[:, :, :, :, i] = probs[:, :, :, :, 0]*lower0 + \
                probs[:, :, :, :, 1]*lower1+probs[:, :, :, :, 2]*lower2
        del probs, lower0, lower1, lower2

        precise = 16
        cdf_m = lower.data.cpu().numpy()*((1 << precise) - (Max_Main -
                                                            Min_Main + 1))  # [1, c, h, w ,Max-Min+1]
        cdf_m = cdf_m.astype(np.int32) + sample.cpu().numpy().astype(np.int32) - Min_Main
        if header.USE_ACCELERATION:
            cdf_main = np.reshape(cdf_m, [len(Effective_Datas), -1])
            Cdf_lower = list(map(lambda x, y: int(y[x - Min_Main]), Effective_Datas, cdf_main))
            Cdf_upper = list(map(lambda x, y: int(
                y[x - Min_Main]), Effective_Datas, cdf_main[:, 1:]))
        else:
            cdf_main = np.reshape(cdf_m, [len(Datas), -1])
            # Cdf[Datas - Min_V]
            Cdf_lower = list(map(lambda x, y: int(y[x - Min_Main]), Datas, cdf_main))
            # Cdf[Datas + 1 - Min_V]
            Cdf_upper = list(map(lambda x, y: int(
                y[x - Min_Main]), Datas, cdf_main[:, 1:]))

        AE.encode_cdf(Cdf_lower, Cdf_upper, f"{bin_dir_path}/main.bin")
        FileSizeMain = os.path.getsize(f"{bin_dir_path}/main.bin")
        print("main.bin: %d bytes" % (FileSizeMain))

        if header.USE_MULTI_HYPER:
            y_hyper_2_q, hyper_2_dec = bits2bin_varlist[3], bits2bin_varlist[4]
            # Hyper 1 Arith Encode
            Datas = torch.reshape(y_hyper_q, [-1]).cpu().numpy().astype(np.int).tolist()
            Max_HYPER_1 = max(Datas)
            Min_HYPER_1 = min(Datas)
            sample = np.arange(Min_HYPER_1, Max_HYPER_1+1+1)  # [Min_V - 0.5 , Max_V + 0.5]
            _, c, h, w = y_hyper_q.shape
            print("Hyper 1 Channel:", c)
            sample = torch.FloatTensor(np.tile(sample, [1, c, h, w, 1])).cuda()

            mean = hyper_2_dec[:, :c, :, :]
            scale = hyper_2_dec[:, c:, :, :]

            scale = torch.abs(scale)
            scale[scale < 1e-6] = 1e-6

            m = torch.distributions.normal.Normal(mean, scale)
            lower = torch.zeros(1, c, h, w, Max_HYPER_1-Min_HYPER_1+2).cuda()
            for ii in range(sample.shape[4]):
                lower[:,:,:,:,ii] = m.cdf(sample[:,:,:,:,ii]-0.5)
            precise = 16
            cdf_m = lower.data.cpu().numpy()*((1 << precise) - (Max_HYPER_1 -
                                                                Min_HYPER_1 + 1))  # [1, c, h, w ,Max-Min+1]
            cdf_m = cdf_m.astype(np.int32) + sample.cpu().numpy().astype(np.int32) - Min_HYPER_1
            cdf_main = np.reshape(cdf_m, [len(Datas), -1])

            # Cdf[Datas - Min_V]
            Cdf_lower = list(map(lambda x, y: int(y[x - Min_HYPER_1]), Datas, cdf_main))
            # Cdf[Datas + 1 - Min_V]
            Cdf_upper = list(map(lambda x, y: int(
                y[x - Min_HYPER_1]), Datas, cdf_main[:, 1:]))
            AE.encode_cdf(Cdf_lower, Cdf_upper, f"{bin_dir_path}/hyper_1.bin")
            FileSizeHyper1 = os.path.getsize(f"{bin_dir_path}/hyper_1.bin")
            print("hyper_1.bin: %d bytes" % (FileSizeHyper1))

            # Hyper 2 Arith Encode
            Min_HYPER_2 = torch.min(y_hyper_2_q).cpu().numpy().astype(np.int).tolist()
            Max_HYPER_2 = torch.max(y_hyper_2_q).cpu().numpy().astype(np.int).tolist()
            _, c, h, w = y_hyper_2_q.shape
            # print("Hyper Channel:", c)
            Datas_hyper = torch.reshape(
                y_hyper_2_q, [c, -1]).cpu().numpy().astype(np.int).tolist()
            # [Min_V - 0.5 , Max_V + 0.5]
            sample = np.arange(Min_HYPER_2, Max_HYPER_2+1+1)
            sample = np.tile(sample, [c, 1, 1])
            lower = torch.sigmoid(self.factorized_entropy_func._logits_cumulative(
                torch.FloatTensor(sample).cuda() - 0.5, stop_gradient=False))

            cdf_h = lower.data.cpu().numpy()*((1 << precise) - (Max_HYPER_2 -
                                                                Min_HYPER_2 + 1))  # [N1, 1, Max-Min+1]
            cdf_h = cdf_h.astype(np.int) + sample.astype(np.int) - Min_HYPER_2
            cdf_hyper = np.reshape(np.tile(cdf_h, [len(Datas_hyper[0]), 1, 1, 1]), [
                                len(Datas_hyper[0]), c, -1])

            # Datas_hyper [256, N], cdf_hyper [256,1,X]
            Cdf_0, Cdf_1 = [], []
            for i in range(c):
                Cdf_0.extend(list(map(lambda x, y: int(
                    y[x - Min_HYPER_2]), Datas_hyper[i], cdf_hyper[:, i, :])))   # Cdf[Datas - Min_V]
                Cdf_1.extend(list(map(lambda x, y: int(
                    y[x - Min_HYPER_2]), Datas_hyper[i], cdf_hyper[:, i, 1:])))  # Cdf[Datas + 1 - Min_V]
            AE.encode_cdf(Cdf_0, Cdf_1, f"{bin_dir_path}/hyper_2.bin")
            FileSizeHyper2 = os.path.getsize(f"{bin_dir_path}/hyper_2.bin")
            print("hyper_2.bin: %d bytes" % (FileSizeHyper2))

            if header.USE_GEO:
                Head_block = struct.pack('6h3IB', Min_Main, Max_Main, Min_HYPER_1, Max_HYPER_1,Min_HYPER_2,Max_HYPER_2, FileSizeMain, FileSizeHyper1, FileSizeHyper2, header.geo_index)
            else:
                Head_block = struct.pack('6h3I', Min_Main, Max_Main, Min_HYPER_1, Max_HYPER_1,Min_HYPER_2,Max_HYPER_2, FileSizeMain, FileSizeHyper1, FileSizeHyper2)

        else:
            # Hyper Arith Encode
            Min_V_HYPER = torch.min(y_hyper_q).cpu().numpy().astype(np.int).tolist()
            Max_V_HYPER = torch.max(y_hyper_q).cpu().numpy().astype(np.int).tolist()
            _, c, h, w = y_hyper_q.shape
            # print("Hyper Channel:", c)
            Datas_hyper = torch.reshape(
                y_hyper_q, [c, -1]).cpu().numpy().astype(np.int).tolist()
            # [Min_V - 0.5 , Max_V + 0.5]
            sample = np.arange(Min_V_HYPER, Max_V_HYPER + 1 + 1)
            sample = np.tile(sample, [c, 1, 1])
            sample_tensor = torch.FloatTensor(sample)
            if header.GPU:
                sample_tensor = sample_tensor.cuda()
            lower = torch.sigmoid(self.factorized_entropy_func._logits_cumulative(
                sample_tensor - 0.5, stop_gradient=False))
            cdf_h = lower.data.cpu().numpy() * ((1 << precise) - (Max_V_HYPER -
                                                                Min_V_HYPER + 1))  # [N1, 1, Max-Min+1]
            cdf_h = cdf_h.astype(np.int) + sample.astype(np.int) - Min_V_HYPER
            cdf_hyper = np.reshape(np.tile(cdf_h, [len(Datas_hyper[0]), 1, 1, 1]), [
                len(Datas_hyper[0]), c, -1])

            # Datas_hyper [256 N], cdf_hyper [256,1,X]
            Cdf_0, Cdf_1 = [], []
            for i in range(c):
                Cdf_0.extend(list(map(lambda x, y: int(
                    y[x - Min_V_HYPER]), Datas_hyper[i], cdf_hyper[:, i, :])))  # Cdf[Datas - Min_V]
                Cdf_1.extend(list(map(lambda x, y: int(
                    y[x - Min_V_HYPER]), Datas_hyper[i], cdf_hyper[:, i, 1:])))  # Cdf[Datas + 1 - Min_V]
            AE.encode_cdf(Cdf_0, Cdf_1, f"{bin_dir_path}/hyper.bin")
            FileSizeHyper = os.path.getsize(f"{bin_dir_path}/hyper.bin")
            print("hyper.bin: %d bytes" % (FileSizeHyper))

            if header.USE_GEO:
                Head_block = struct.pack('4h2IB', Min_Main, Max_Main, Min_V_HYPER, Max_V_HYPER,
                                        FileSizeMain, FileSizeHyper, header.geo_index)
            else:
                Head_block = struct.pack('4h2I', Min_Main, Max_Main, Min_V_HYPER, Max_V_HYPER,
                                        FileSizeMain, FileSizeHyper)
        file_object.write(Head_block)  # CU information
        # cat Head_Infor and 2 files together
        # Head = [FileSizeMain,FileSizeHyper,H,W,Min_Main,Max_Main,Min_V_HYPER,Max_V_HYPER,model_index]
        # print("Head Info:",Head)
        with open(f"{bin_dir_path}/main.bin", 'rb') as f:
            bits = f.read()
            file_object.write(bits)

        if header.USE_MULTI_HYPER:
            with open(f"{bin_dir_path}/hyper_1.bin", 'rb') as f:
                bits = f.read()
                file_object.write(bits)
            with open(f"{bin_dir_path}/hyper_2.bin", 'rb') as f:
                bits = f.read()
                file_object.write(bits)
        else:
            with open(f"{bin_dir_path}/hyper.bin", 'rb') as f:
                bits = f.read()
                file_object.write(bits)

        ## added by sxd
        # file_object.close()
        os.remove(f"{bin_dir_path}/main.bin")
        os.remove(f"{bin_dir_path}/hyper_1.bin")
        os.remove(f"{bin_dir_path}/hyper_2.bin")
        return file_object