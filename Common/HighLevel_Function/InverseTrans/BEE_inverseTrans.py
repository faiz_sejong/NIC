import torch.nn as nn
import torch
import torch.nn.functional as F
from Common.models.BEE_models.layers import (
    AttentionBlock,
    ResidualBlock,
    ResidualBlockUpsample,
    subpel_conv3x3,
)


MeanAndResidualScale = True
class BEE_InverseTrans(nn.Module):
    def __init__(self, N=192, M=192, **kwargs):
        super().__init__()
        device = 'cpu' if kwargs.get("device") == None else kwargs.get("device")
        self.N = int(N)
        self.M = int(M)
        self.DeterminismSpeedup = True
        self.decSplit1 = 1
        self.decSplit2 = 1
        self.g_s = nn.Sequential(
            ResidualBlock(N, N, device = device),
            ResidualBlockUpsample(N, N, 2, device = device),
            ResidualBlock(N, N, device = device),
            ResidualBlockUpsample(N, N, 2, device = device),
            AttentionBlock(N, device = device),
            ResidualBlock(N, N, device = device),
        )
        self.g_s_extension = nn.Sequential(
            ResidualBlockUpsample(N, N, 2, device = device),
            ResidualBlock(N, N, device = device),
            subpel_conv3x3(N, 3, 2, device = device),
        )

    @staticmethod
    def splitFunc(x, func, splitNum, pad, crop):
        _, _, _, w = x.shape
        w_step = ((w + splitNum - 1) // splitNum)
        pp = []
        for i in range(splitNum):
            start_offset = pad if i > 0 else 0
            start_crop = crop if i > 0 else 0
            end_offset = pad if i < (splitNum - 1) else 0
            end_crop = crop if i < (splitNum - 1) else 0
            dummy = func(x[:, :, :, (w_step * i) - start_offset:w_step * (i + 1) + end_offset])
            dummy = dummy[:, :, :, start_crop:]
            if end_crop > 0:
                dummy = dummy[:, :, :, :-end_crop]
            pp.append(dummy)
        x_hat = torch.cat(pp, dim = 3)
        return x_hat

    def SynTrans(self,quant_latent): #->Tensor imArray
        f_map = self.DecoderFirstPart(quant_latent,self.decSplit1)
        imArray = self.DecoderSecondPart(f_map,self.decSplit2)
        return imArray

    def DecoderFirstPart(self,quant_latent,num_first_level_tile): #-> Tensor f_map
        f_map = self.splitFunc(quant_latent, self.g_s, num_first_level_tile, 4, 16)
        return f_map

    def DecoderSecondPart(self,f_map,num_second_level_tile):#-> Tensor imArray
        imArray = self.splitFunc(f_map, self.g_s_extension, num_second_level_tile, 4, 16).clamp_(0, 1)
        return imArray
    
    def decode(self, quant_latent, header):
        torch.cuda.empty_cache()
        torch.backends.cudnn.deterministic = False if self.DeterminismSpeedup else True
        if quant_latent.is_cuda:
            self.g_s.half()
            self.g_s_extension.half()
            quant_latent = quant_latent.half()
        imArray = self.SynTrans(quant_latent)
        if quant_latent.is_cuda:
            imArray = imArray.to(torch.float32)
            self.g_s.to(torch.float32)
            self.g_s_extension.to(torch.float32)
        torch.backends.cudnn.deterministic = True
        return imArray

    def load_state_dict(self, state_dict, strict=False):
        state_dict_new = state_dict.copy()
        exclude = ["g_s.6", "g_s.7", "g_s.8"]
        include = ["g_s_extension.0", "g_s_extension.1", "g_s_extension.2"]
        for k in state_dict:
            for idx, j in enumerate(exclude):
                if j in k:
                    k_new = k.replace(j, include[idx])
                    state_dict_new[k_new] = state_dict_new[k]
                    del state_dict_new[k]
        state_dict = state_dict_new
        super().load_state_dict(state_dict, strict=strict)
    
    def update(self, header):
        self.DeterminismSpeedup = header.picture.picture_header.deterministic_processing_flag
        self.decSplit1 = header.picture.picture_header.num_first_level_tile
        self.decSplit2 = header.picture.picture_header.num_second_level_tile
